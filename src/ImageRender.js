import React, { Component } from 'react';

class ImageRender extends Component {
    state = {
        principalName: '',
        imageUrl: ''
    }

    render() {
        return (
            <div className="images-list">
              <h1>Loaded Images {this.props.name}</h1>
              <ul>
                <li><div><img src={this.props.first} alt="First" /></div></li>
                <li><div><img src="https://typ-media-storage.s3.eu-west-1.amazonaws.com/test-image.png" alt="Second" /></div></li>
                <li><div><img src="https://typ-media-storage.s3.eu-west-1.amazonaws.com/test-image.png" alt="Third" /></div></li>
              </ul>
            </div>
          );
    }
}

export default ImageRender;