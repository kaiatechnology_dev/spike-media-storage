import useImage from 'react-image/useImage'
 
export default function MyComponent() {
  const {src, isLoading, error} = useImage({
    srcList: 'https://typ-media-storage.s3.eu-west-1.amazonaws.com/test-img.pn?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAVLDN2GQT3G4LHNWK%2F20200518%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Date=20200518T161923Z&X-Amz-Expires=10&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEJH%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCWV1LXdlc3QtMSJHMEUCIASnqVU56Kg%2FAigk0F8XgRQ0d0wN2%2BeF6l2MBupz3lB9AiEA25qM7eKzvOdmpnKbVg2M46ooE9MVAFpE3WWBmUAKQcQq2QEI2f%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARABGgwzNjc0NTAwMTA2NjMiDJxt3snOFkyimIM40SqtAQnTOsAP1stagK1QLO23mRAemHIBhNMUbtqico7Ssut12ap%2BeWWNNt0tBUT3uQdG4XvtQLCBWftvF8l255dXphp%2F4Ga5WwbCTiiMsn2Gb1NopzF48oqsrAMHxYxuRd0sh%2BVM6hv62%2BVKprvKJs7f5tz3%2F3j41jl5wZE7ATPvvJKp5GEq6Q4DBkLGem2D1fDQPrPwg0JWkUDJroAvLRNqdEa%2B1oReQBthcrBZy8kBMIrrivYFOuAB%2FUZBCTQN93r1f3iWSqV0RpkV9rSWa0aNBF6%2F118S6z%2F8T6xyrZRPAwUhc3CgugMhXsKHrag0PCk1JeToDEHxRz0dRDbQrNkVdMGHOHwubgkt8vhXaa1trSRglENKNUkF21oyYy0dTuOMVlk5MFHzSR5RMCRug6W4EOkYoaxKGle9rFgDYAnUPJvkST7CXN1%2BAsFt3hnn8JkTqQNKIvnwTi%2F%2BLUhUE%2BD5lyj9LpniDzZb5t%2BR0%2FAYfZn4jQlQ9Y4Ts8GTWVI%2BrU4D%2FRJ%2BSPDvu7jGLgx3hMDbCFSjAXHDIaU%3D&X-Amz-Signature=695a369152c6c73e18fabf3f006a799a708a6b8153677323ed6b956b87a23496&X-Amz-SignedHeaders=host',
  })
 
  return <img src={src} />
}