import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import axios from 'axios';
import ImageRenderer from './ImageRender'
import LoginForm from './LoginForm'
import FormData from 'form-data'

var cors = require('cors')

const UserPoolId = 'eu-west-1_WQsfQqoEY';
const ClientId = '1l80b6o3o5lt78kra09mnotkfg';
const ApiGatewayUrl = 'https://q1byfted4f.execute-api.eu-west-1.amazonaws.com/dev/upload-image';

const userPool = new CognitoUserPool({
  UserPoolId: UserPoolId,
  ClientId: ClientId,
});

let dynamicUrl = "";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      accessToken: 'typ-token',
      isAuthenticated: false,
      isLoginFailed: false,
    };
  };

  // is used by both login and password reset
  onSuccess = (result) => {
    console.log("onSuccess");
    console.log(result);
    this.setState({
      accessToken: result.idToken.jwtToken, // the token used for subsequent, authorized requests
      isAuthenticated: true,
      isLoginFailed: false,
    });
  };

  // is used by both login and password reset
  onFailure = (error) => {
    console.log("onFailure");
    console.log(error);
    this.setState({
      isAuthenticated: false,
      isLoginFailed: true,
      statusCode: '',
    });
  };

  onSubmit = (event) => {
    event.preventDefault();

    let cognitoUser = new CognitoUser({
      Username: this.state.username,
      Pool: userPool,
    });

    const authenticationDetails = new AuthenticationDetails({
      Username: this.state.username,
      Password: this.state.password,
    });

    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: this.onSuccess,
      onFailure: this.onFailure,
      newPasswordRequired: (userAttributes, requiredAttributes) => {
        console.log("newPasswordRequired");
        console.log(userAttributes);

        // not interesting for this demo - add a bogus e-mail and append an X to the initial password
        userAttributes['email'] = 'alex.gerasimenko@kaiatechnology.ie';
        cognitoUser.completeNewPasswordChallenge(this.state.password + 'X', userAttributes, this);
      },
    });
  };

  onDrop = (files) => {
    console.log("MyToken:" + this.state.accessToken);
    let form = new FormData();
    //let boundary = form.getBoundary()
    //console.log("Boundary:" + boundary);

    form.append('file', files[0], files[0].fileName);

    axios.post(ApiGatewayUrl, form, {
      headers: {
        'accept': 'application/json',
        'Accept-Language': 'en-US,en;q=0.8',
        'Content-Type': `multipart/form-data; boundary=WebKitFormBoundaryCIkXuNWC8OhEuT3S`,
        Authorization: this.state.accessToken
      },
      params : {
        principal: "bay2",
        venue: "venue4"
      }
    })
      .then((response) => {
        console.log("Ok. Got response for load " + response);
      }).catch((error) => {
        console.log("Error. Got response for load " + error);
      });
  };

  render() {
    return (
      <div>
        <h1>Login to upload files</h1>
        <form onSubmit={this.onSubmit}>
          <input type='text' value={this.state.username} onChange={(event) => this.setState({ username: event.target.value })} placeholder='username' /><br />
          <input type='password' value={this.state.password} onChange={(event) => this.setState({ password: event.target.value })} placeholder='password' /><br />
          <input type='submit' value='Login' />
        </form>
        <p style={{ color: 'red', display: this.state.isLoginFailed ? 'block' : 'none' }}>Credentials incorrect</p>
        <div style={{ display: this.state.isAuthenticated ? 'block' : 'none' }}>
          <Dropzone onDrop={this.onDrop}>
            <p>Drop your files here or click to select one.</p>
          </Dropzone>
          <p>Status Code: {this.state.statusCode}</p>
        </div>

        <br />
        <div>
          <ImageRenderer first="https://typ-media-storage.s3.eu-west-1.amazonaws.com/test-image-2.png" />
        </div>
      </div>
    );
  };
};
