import  React, {Component } from 'react';

class LoginForm extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            
        }
    }

    render() {
        return (
            <div>
            <h1>Login to upload files</h1>
            <form onSubmit={this.onSubmit}>
                <input type='text' value={this.state.username} onChange={(event) => this.setState({ username: event.target.value })} placeholder='username' /><br />
                <input type='password' value={this.state.password} onChange={(event) => this.setState({ password: event.target.value })} placeholder='password' /><br />
                <input type='submit' value='Login' />
            </form>
            <p style={{ color: 'red', display: this.state.isLoginFailed ? 'block' : 'none' }}>Credentials incorrect</p>
        </div>
          );
    }
}

export default LoginForm;