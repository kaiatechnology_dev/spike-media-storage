import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import axios from 'axios';
import ImageRenderer from './ImageRender'
import LoginForm from './LoginForm'

var cors = require('cors')

const UserPoolId = 'eu-west-1_WQsfQqoEY';
const ClientId = '1l80b6o3o5lt78kra09mnotkfg';
const ApiGatewayUrl = 'https://j8zytbwd83.execute-api.eu-west-1.amazonaws.com/production/venue-images-places';

const userPool = new CognitoUserPool({
  UserPoolId: UserPoolId,
  ClientId: ClientId,
});

let dynamicUrl = "";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      accessToken: 'typ-token',
      isAuthenticated: false,
      isLoginFailed: false,
    };
  };

  // is used by both login and password reset
  onSuccess = (result) => {
    console.log("onSuccess");
    console.log(result);
    this.setState({
      accessToken: result.idToken.jwtToken, // the token used for subsequent, authorized requests
      isAuthenticated: true,
      isLoginFailed: false,
    });
  };

  // is used by both login and password reset
  onFailure = (error) => {
    console.log("onFailure");
    console.log(error);
    this.setState({
      isAuthenticated: false,
      isLoginFailed: true,
      statusCode: '',
    });
  };

  onSubmit = (event) => {
    event.preventDefault();

    let cognitoUser = new CognitoUser({
      Username: this.state.username,
      Pool: userPool,
    });

    const authenticationDetails = new AuthenticationDetails({
      Username: this.state.username,
      Password: this.state.password,
    });

    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: this.onSuccess,
      onFailure: this.onFailure,
      newPasswordRequired: (userAttributes, requiredAttributes) => {
        console.log("newPasswordRequired");
        console.log(userAttributes);

        // not interesting for this demo - add a bogus e-mail and append an X to the initial password
        userAttributes['email'] = 'alex.gerasimenko@kaiatechnology.ie';
        cognitoUser.completeNewPasswordChallenge(this.state.password + 'X', userAttributes, this);
      },
    });
  };

  onDrop = (files) => {
    axios.defaults.headers.get['Content-Type'] = 'application/json;charset=utf-8';
    //axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';

    //axios.defaults.headers.put['Content-Type'] = 'images/*';
    axios.defaults.headers.put['Access-Control-Allow-Origin'] = '*';



    console.log("MyToken:" + this.state.accessToken);
    // first get the pre-signed URL
    axios.get(ApiGatewayUrl + "?principal=bay25&venue=ven6&amount=2",
      {
        /*        params: {
                  principal: "bay56",
                  venue: "ven5",
                  amount: 3
                },*/
        headers: {
          Authorization: this.state.accessToken
        }
      })
      .then((response) => {

        dynamicUrl = response.body;
        console.log("Got response for signed URL:" + dynamicUrl);

        // now do a PUT request to the pre-signed URL
        axios.put(dynamicUrl,
          files[0],
          {
            headers: {
              'Access-Control-Allow-Origin': '*'
            }
          }
          )
          .then((response) => {
            console.log("Response:" + response.body);
            this.setState({
              statusCode: response.status,
              imageUrl: dynamicUrl
            });
          })
          .catch((err) => {
            console.log("ERROR IN PUT: " + err);
          });
      })
      .catch((error) => {
        console.log("Error in GET:" + error);
      });
  };

  render() {
    return (
      <div>
        <h1>Login to upload files</h1>
        <form onSubmit={this.onSubmit}>
          <input type='text' value={this.state.username} onChange={(event) => this.setState({ username: event.target.value })} placeholder='username' /><br />
          <input type='password' value={this.state.password} onChange={(event) => this.setState({ password: event.target.value })} placeholder='password' /><br />
          <input type='submit' value='Login' />
        </form>
        <p style={{ color: 'red', display: this.state.isLoginFailed ? 'block' : 'none' }}>Credentials incorrect</p>
        <div style={{ display: this.state.isAuthenticated ? 'block' : 'none' }}>
          <Dropzone onDrop={this.onDrop}>
            <p>Drop your files here or click to select one.</p>
          </Dropzone>
          <p>Status Code: {this.state.statusCode}</p>
        </div>

        <br />
        <div>
          <ImageRenderer first="https://typ-media-storage.s3.eu-west-1.amazonaws.com/test-image-2.png" />
        </div>
      </div>
    );
  };
};
